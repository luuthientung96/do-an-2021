const { find } = require('../controller/model/user');
const User = require('../controller/model/user');
const jwt = require('jsonwebtoken');
const redis = require("redis");
const redisClient = redis.createClient("http://localhost:6379");
module.exports.requireAuth = function(req, res, next) {
    const urlp = req.protocol + "://" + req.get('host') + req.originalUrl;
    if (!req.signedCookies.userId || !req.signedCookies.token) {
        res.redirect(`http://localhost:8081?request=${urlp}&check=login`);
        return;
    } else {
        const TOKEN_SECRET = "ZmJjYzJkYmU2YTRhNmNlZDE0NWE2NDk0OTQxYjRjNjJkYmNjMWEyYmE3NjIyMjc4MmM1ZjIyZTU0NTUwMzE0NTExYzZkODU4MmRjMjdhMjM3Y2U1NWE3NWE5ODUzOTA0OGUyY2Q2MWVkYzRhYjk4MWIxMGEzYjZiMWVmYWUxNzc="
        const token = req.signedCookies.token;
        const request = req.query.request;
        redisClient.get(token + "", (err, data) => {
            if (data) {
                res.redirect(`http://localhost:8081/?request=${urlp}&check=login`);
                return;
            }
        })
        const secret = new Buffer(TOKEN_SECRET, "base64")
        User.find({ _id: req.signedCookies.userId })
            .then(user => {
                if (!user) {
                    res.redirect('/');
                    return;
                }
                if (user.position !== 'admin') {
                    jwt.verify(token, secret, (err, val) => {
                        if (err) {
                            res.redirect('/');
                            return;
                        }
                    })
                }
            });
    }

    next();
};

module.exports.requireAdmin = function(req, res, next) {
    User.findOne({ _id: req.signedCookies.userId })
        .then(user => {
            if (user.position !== 'admin') {
                redirect('/')
                return;
            }
        })
    next();
};

module.exports.requireLogin = function(req, res, next) {
    if (req.signedCookies.userId) {
        res.redirect('/');
        return;
    }
    next();
};

module.exports.requireUser = function(req, res, next) {
    if (req.signedCookies.userId) {
        User.findOne({ _id: req.signedCookies.userId })
            .then(data => {
                res.locals.name = data.fullName;
            })
    }
    next();
};

module.exports.requireAdminLogin = function(req, res, next) {
    if (req.signedCookies.userId) {
        User.findOne({ _id: req.signedCookies.userId })
            .then(data => {
                if (data.position == 'admin') res.locals.admin = true;
            })
    }
    next();
};

module.exports.requireUserlogin = function(req, res, next) {
    if (req.signedCookies.userId) {
        res.locals.login = true;
    }
    next();
};