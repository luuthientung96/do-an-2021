const express = require('express');
const router = express.Router();
const siteController = require('../controller/siteController');
const authMiddlewares = require('../middlewares/authmiddlewares');


router.get('/profile', authMiddlewares.requireAuth, siteController.profile);
router.get('/backend', siteController.backend);
router.get('/frontend', siteController.frontend);
router.get('/logout', siteController.logout);
router.get('/logout/sso', siteController.logoutsso);
router.post('/login', siteController.checklogin);
router.get('/check-login', siteController.checklogin);
router.post('/signup', siteController.checksignup);
router.get('/course', siteController.course);
router.get('/signup', authMiddlewares.requireLogin, siteController.signup);
router.get('/login', authMiddlewares.requireLogin, siteController.login);
router.get('/login/sso', siteController.authenticateToken);
router.get('/', siteController.index);

module.exports = router;