package com.project.Login.validate;

import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class FormValidateException extends BaseException {
    private static final long serialVersionUID = 1L;

    private static String buildMessage(Map<String, Object> fieldErrors) {
        if (fieldErrors == null || fieldErrors.isEmpty()) {
            return null;
        }
        return fieldErrors.entrySet().stream()
                .map(entry -> String.format("%s: %s", entry.getKey(), entry.getValue()))
                .collect(Collectors.joining(", "));
    }

    private static String buildMessage(BindingResult bindingResult) {
        if (bindingResult == null || !bindingResult.hasFieldErrors()) {
            return null;
        }
        return bindingResult.getFieldErrors().stream()
                .map(fieldError -> String.format("%s: %s", fieldError.getField(), fieldError.getDefaultMessage()))
                .collect(Collectors.joining(", "));
    }

    private Map<String, Object> buildFieldErrors(BindingResult bindingResult) {
        Map<String, Object> fieldErrors = new HashMap<>();
        if (bindingResult == null || !bindingResult.hasFieldErrors()) {
            return fieldErrors;
        }
        return bindingResult.getFieldErrors().stream()
                .collect(Collectors.toMap(FieldError::getField, DefaultMessageSourceResolvable::getDefaultMessage));
    }
}
