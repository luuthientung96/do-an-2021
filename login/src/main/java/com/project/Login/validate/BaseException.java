package com.project.Login.validate;

public class BaseException extends RuntimeException {
    public BaseException(String message) {
        super(message);
    }

    public BaseException() {
        super("Exception");
    }
}
