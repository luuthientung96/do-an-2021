package com.project.Login.validate;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(
        value = HttpStatus.NOT_FOUND,
        reason = "Item not found"
)
public class NotFoundException extends BaseException {
    private static final long serialVersionUID = 1L;

    public NotFoundException() {
    }
}
