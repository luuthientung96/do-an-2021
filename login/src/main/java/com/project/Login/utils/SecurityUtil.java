package com.project.Login.utils;

import com.project.Login.dto.user.UserDTO;
import com.project.Login.modal.User;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import java.security.Principal;

public class SecurityUtil {
    public static User getUser(Principal currentUser) {
        var principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            UserDTO userDetail = (UserDTO) ((PreAuthenticatedAuthenticationToken) currentUser).getPrincipal();
            if (currentUser != null) {
                return userDetail.getUser();
            }
        }
        return null;
    }
}
