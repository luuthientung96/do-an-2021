package com.project.Login.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.project.Login.config.filter.ResettableStreamHttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;


public class RequestUtil {
    public static String getBody(HttpServletRequest request) throws IOException {
        String body = null;
        if (request instanceof ResettableStreamHttpServletRequest) {
            ResettableStreamHttpServletRequest wrappedRequest = (ResettableStreamHttpServletRequest)request;
            body = (String)wrappedRequest.getReader().lines().reduce("", (accumulator, actual) -> {
                return accumulator + actual;
            });
            return body;
        } else {
            if (request instanceof SecurityContextHolderAwareRequestWrapper) {
                SecurityContextHolderAwareRequestWrapper securityWrap = (SecurityContextHolderAwareRequestWrapper)request;
                body = (String)securityWrap.getReader().lines().reduce("", (accumulator, actual) -> {
                    return accumulator + actual;
                });
            }

            return body;
        }
    }

    public static boolean existField(JsonNode node, String key) {
        if (StringUtils.isBlank(key)) {
            return false;
        } else {
            boolean found = false;
            if (key.contains(".")) {
                String[] lstField = key.split(".");
                JsonNode tempNode = node.path(lstField[0]);
                found = !tempNode.isMissingNode();
                if (found && lstField.length > 1) {
                    for(int index = 1; found && index < lstField.length; ++index) {
                        tempNode = tempNode.path(lstField[index]);
                        found = !tempNode.isMissingNode();
                    }
                }
            } else {
                found = !node.path(key).isMissingNode();
            }

            return found;
        }
    }
}
