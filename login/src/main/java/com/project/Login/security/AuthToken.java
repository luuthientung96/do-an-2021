package com.project.Login.security;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthToken {
    private String token;
    private String redirect;

    public AuthToken(String token, String redirect) {
        this.token = token;
        this.redirect = redirect;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRedirect() {
        return redirect;
    }

    public void setRedirect(String redirect) {
        this.redirect = redirect;
    }
}
