package com.project.Login.modal;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "reset_password")
@Getter
@Setter
public class ResetPassword {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "active")
    private boolean active;

    @Column(name = "token")
    private String token;

    @Column(name = "type")
    private String type;

    @Column(name = "type_id")
    private int typeId;

    @Column(name = "created_on")
    private Instant createdOn;
}
