package com.project.Login.dto.user.request;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.util.Set;

@Getter
@Setter
public class RegisterUserRequest {
    @NotNull
    @Size(min = 1, max = 50)
    private String login;

    @NotNull
    private String password;

    @Size(max = 50)
    private String firstName;

    @Size(max = 50)
    private String lastName;

    private String socialId;

    private String phoneNumber;

    @Email
    @Size(min = 5, max = 254)
    private String email;
    private Set<String> authorities;
}
