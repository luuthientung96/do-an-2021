package com.project.Login.dto.user.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Getter
@Setter
public class UserRequest {
    @NotNull
    @Size(max = 50, min = 4)
    private String username;
    @NotNull
    @Size(max = 50, min = 4)
    private String password;
    private Set<String> authorities;
}
