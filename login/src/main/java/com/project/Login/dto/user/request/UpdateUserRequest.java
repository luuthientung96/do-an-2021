package com.project.Login.dto.user.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.project.Login.modal.Authority;
import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class UpdateUserRequest {
    private Long id;

    @Size(max = 50)
    @Column(name = "first_name", length = 50)
    private String firstName;

    @Size(max = 50)
    private String lastName;

    private String password;

    @Email
    @Size(min = 5, max = 254)
    private String email;

    @NotNull
    private boolean activated = false;

    @Size(max = 20)
    private String activationKey;

    @Size(max = 20)
    private String resetKey;

    private Set<Authority> authorities = new HashSet<>();
}
