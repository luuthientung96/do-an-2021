package com.project.Login.dto.user.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChangePasswordRequest {
    private String login;
    private String password;
    private String newPassword;
}
