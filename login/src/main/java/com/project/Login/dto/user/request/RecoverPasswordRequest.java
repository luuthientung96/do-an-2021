package com.project.Login.dto.user.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecoverPasswordRequest {
    private String login;
    private String password;
    private String token;
}
