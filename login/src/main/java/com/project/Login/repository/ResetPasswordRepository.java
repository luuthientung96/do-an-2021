package com.project.Login.repository;

import com.project.Login.modal.ResetPassword;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ResetPasswordRepository extends JpaRepository<ResetPassword, Integer> {
    Optional<ResetPassword> findByTokenAndTypeIdAndActiveFalse(String token, int id);
}
