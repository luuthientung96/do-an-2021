package com.project.Login.repository;

import com.project.Login.modal.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthoriryRepository extends JpaRepository<Authority, String> {
}
