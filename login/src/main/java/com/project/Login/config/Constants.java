package com.project.Login.config;

public class Constants {
    public static final int ACTIVE_STATUS = 1;
    public static final int INACTIVE = 0;

    public static final long ACCESS_TOKEN_VALIDITY = 1000 * 60 * 60 * 24 * 365;//1H
    public static final String SIGNING_KEY = "ZmJjYzJkYmU2YTRhNmNlZDE0NWE2NDk0OTQxYjRjNjJkYmNjMWEyYmE3NjIyMjc4MmM1ZjIyZTU0NTUwMzE0NTExYzZkODU4MmRjMjdhMjM3Y2U1NWE3NWE5ODUzOTA0OGUyY2Q2MWVkYzRhYjk4MWIxMGEzYjZiMWVmYWUxNzc=";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String AUTHORITIES_KEY = "scope";

    public static final String QUEUE_NAME = "queue_name";
    public static final String EXCHANGE_NAME = "exchange_name";
    public static final String ROUTER_KEY = "rabbitmq.*";

    public static final String USER = "user";
}
