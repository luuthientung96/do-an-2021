package com.project.Login.service;

import com.project.Login.dto.user.request.ChangePasswordRequest;
import com.project.Login.dto.user.request.RecoverPasswordRequest;
import com.project.Login.dto.user.request.RegisterUserRequest;
import com.project.Login.modal.Authority;
import com.project.Login.modal.ResetPassword;
import com.project.Login.modal.User;
import com.project.Login.repository.ResetPasswordRepository;
import com.project.Login.repository.UserRepository;
import com.project.Login.utils.Utils;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.time.Instant;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final JavaMailSender sendEmail;
    private final PasswordEncoder passwordEncoder;
    private final ResetPasswordRepository resetPasswordRepository;

    public UserService(UserRepository userRepository, JavaMailSender sendEmail, PasswordEncoder passwordEncoder, ResetPasswordRepository resetPasswordRepository) {
        this.userRepository = userRepository;
        this.sendEmail = sendEmail;
        this.passwordEncoder = passwordEncoder;
        this.resetPasswordRepository = resetPasswordRepository;
    }

    @Async
    public void sendMail(SimpleMailMessage message){
        sendEmail.send(message);
    }

    public User registerUser(RegisterUserRequest request) {
        User user = new User();
        user.setLogin(request.getLogin());
        user.setEmail(request.getEmail());
        user.setFirstName(request.getFirstName());
        user.setLastName(request.getLastName());
        user.setSocialId(request.getSocialId());
        String pass = passwordEncoder.encode(request.getPassword());
        user.setPassword(pass);
        Set<Authority> authorities = new HashSet<>();

        for (String auth : request.getAuthorities()) {
            Authority authority = new Authority();
            authority.setName(auth);
            authorities.add(authority);
        }
        user.setAuthorities(authorities);
        return userRepository.save(user);

    }

    public User changeUser(ChangePasswordRequest model) {
        Optional<User> user = userRepository.findByLogin(model.getLogin());
        if (!user.isPresent()) {
            return null;
        }
        if (!user.get().getPassword().equals(Utils.md5(model.getPassword()))) {
            return null;
        }
        user.get().setPassword(Utils.md5(model.getPassword()));
        return userRepository.save(user.get());
    }

    @Transactional
    public void recoveryPasswordWithEmail(Principal currentUser, String type) throws NoSuchAlgorithmException {
        var user = userRepository.findByLogin(currentUser.getName());
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(user.get().getEmail());
        String random = UUID.randomUUID().toString();
        String token = Utils.md5(random);
        ResetPassword resetPassword = new ResetPassword();
        resetPassword.setToken(token);
        resetPassword.setActive(false);
        resetPassword.setType(type);
        resetPassword.setTypeId(Integer.parseInt(user.get().getId().toString()));
        resetPassword.setCreatedOn(Instant.now());

        resetPasswordRepository.save(resetPassword);


        message.setSubject("Email reset password from hongtruong store");
        message.setText("This link reset password, Do not share this code with anyone:" + token);
        sendEmail.send(message);
    }

    @Transactional
    public void recoverPassword(RecoverPasswordRequest model, User user, ResetPassword resetPassword) {
        userRepository.save(user);
        resetPassword.setActive(true);
        resetPasswordRepository.save(resetPassword);
    }
}
