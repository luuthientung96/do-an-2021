package com.project.Login.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.Login.config.Constants;
import com.project.Login.config.SpeedSMSAPI;
import com.project.Login.dto.user.request.ChangePasswordRequest;
import com.project.Login.dto.user.request.RegisterUserRequest;
import com.project.Login.dto.user.request.UpdateUserRequest;
import com.project.Login.dto.user.request.UserRequest;
import com.project.Login.modal.User;
import com.project.Login.repository.ResetPasswordRepository;
import com.project.Login.repository.UserRepository;
import com.project.Login.security.AuthToken;
import com.project.Login.security.TokenProvider;
import com.project.Login.service.UserService;
import com.project.Login.utils.RequestUtil;
import com.project.Login.validate.FormValidateException;
import com.project.Login.validate.NotFoundException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.io.NotActiveException;
import java.net.URISyntaxException;
import java.security.Principal;
import java.time.Duration;
import java.util.Objects;
import java.util.Random;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/user")
public class UserController {
    private final AuthenticationManagerBuilder authenticationManager;
    private final TokenProvider tokenProvider;
    private final UserService userService;
    private final UserRepository userRepository;
    private final ObjectMapper objectMapper;
    private final ResetPasswordRepository resetPasswordRepository;
    @Autowired
    private SpeedSMSAPI speedSMSAPI;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    public UserController(AuthenticationManagerBuilder authenticationManager, TokenProvider tokenProvider, UserService userService, UserRepository userRepository, ObjectMapper objectMapper, ResetPasswordRepository resetPasswordRepository) {
        this.authenticationManager = authenticationManager;
        this.tokenProvider = tokenProvider;
        this.userService = userService;
        this.userRepository = userRepository;
        this.objectMapper = objectMapper;
        this.resetPasswordRepository = resetPasswordRepository;
    }

    @PostMapping("/register")
    public ResponseEntity<User> Register(@Valid @RequestBody RegisterUserRequest modal, String otp) {
        //check email
        User userExist = userRepository.findByEmail(modal.getEmail()).orElse(null);
        if(userExist != null){
            return  ResponseEntity.status(HttpStatus.CONFLICT).body(null);
        }
        //check-otp
        String otpRedis = redisTemplate.opsForValue().get(modal.getEmail());
        redisTemplate.delete(modal.getEmail());
        if(StringUtils.equals(otp,otpRedis)){
            User user = userService.registerUser(modal);
            return ResponseEntity.ok(user);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @GetMapping("/find")
    public ResponseEntity<?> getUserInfor(Principal principal) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        var result = userRepository.findByLogin(principal.getName()).get();

        return ResponseEntity.ok(result);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getUserById(@PathVariable Long id) {
        User user = userRepository.findById(id).get();

        return ResponseEntity.ok(user);
    }

    @PostMapping("/check")
    public ResponseEntity<?> checkSocialLogin(@RequestBody RegisterUserRequest userRequest, String request, HttpServletResponse response, String redirect) {
        User user = userRepository.findByEmail(userRequest.getEmail()).orElse(null);
        if(user == null){
            user = userService.registerUser(userRequest);
        }
        Authentication authentication = new UsernamePasswordAuthenticationToken(user.getLogin(), null,AuthorityUtils.createAuthorityList("USER") );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final String token = tokenProvider.generateToken(authentication);
        response.addCookie(new Cookie("authenticate", token));
        String red = redirect + "?token="+token+"&request="+request;
        return ResponseEntity.ok(new AuthToken(token , red));
    }

    @PostMapping("/logout")
    public ResponseEntity<?> logout(@RequestHeader("Authorization") String tokenBearer , HttpServletResponse res) throws IOException, URISyntaxException {
        if(tokenBearer != null){
            String token = tokenBearer.replace(Constants.TOKEN_PREFIX, "");
            redisTemplate.opsForValue().set(token,"false");
        }
        clear(res,"token");
        return ResponseEntity.ok(null);
    }

    public void clear(HttpServletResponse httpServletResponse, String name) {
        Cookie cookie = new Cookie(name, null);
        cookie.setPath("/");
        cookie.setHttpOnly(true);
        cookie.setMaxAge(0);
        httpServletResponse.addCookie(cookie);
    }

    @PostMapping("/authenticate")
    public ResponseEntity<?> authenticate(@RequestBody UserRequest userRequest, String request, HttpServletResponse response, String redirect, String otp) throws IOException, URISyntaxException {
        final Authentication authentication = authenticationManager.getObject().authenticate(
                new UsernamePasswordAuthenticationToken(
                        userRequest.getUsername(),
                        userRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        final String token = tokenProvider.generateToken(authentication);
        User userLogin = userRepository.findByLogin(userRequest.getUsername()).orElse(null);
        if(userLogin != null){
            String otpRedis = redisTemplate.opsForValue().get(userLogin.getEmail());
            redisTemplate.delete(userLogin.getEmail());
            if(!StringUtils.equals(otp,otpRedis)){
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        }
        response.addCookie(new Cookie("authenticate", token));
        String red = redirect + "?token="+token+"&request="+request;
        return ResponseEntity.ok(new AuthToken(token , red));
    }

    @GetMapping("/sso_check")
    public ResponseEntity<?> authenticate(String request, HttpServletResponse response, String redirect, String otp,HttpServletRequest httpServletRequest) throws IOException, URISyntaxException {
        String header = httpServletRequest.getHeader(Constants.HEADER_STRING);
        String token = header.replace(Constants.TOKEN_PREFIX, "");
        String red = redirect + "?token="+token+"&request="+request;
        return ResponseEntity.ok(new AuthToken(token , red));
    }

    @PostMapping("/sms/generate")
    public ResponseEntity<?> generateOtp(@RequestParam("email") String email ) throws IOException {
        User userExist = userRepository.findByEmail(email).orElse(null);
        if(userExist != null){
            return  ResponseEntity.status(HttpStatus.CONFLICT).body(null);
        }
        int number =  new Random().nextInt(900000) + 100000;
        redisTemplate.delete(email);
        redisTemplate.opsForValue().set(email,String.valueOf(number), Duration.ofSeconds(900));
//        var res = speedSMSAPI.sendSMS(phoneNumber, number+"", 2, "0339145511");
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(email);
        message.setSubject("Mã otp đăng ký tài khoản elearning");
        message.setText("Không gửi mã này cho bât kỳ ai, mã chỉ tồn tại trong 15 phút:" + number);
        userService.sendMail(message);
        return ResponseEntity.ok("success");
    }

    @PostMapping("/otp/login")
    public ResponseEntity<?> generateOtpLogin(@RequestBody UserRequest userRequest) throws IOException {
        final Authentication authentication = authenticationManager.getObject().authenticate(
                new UsernamePasswordAuthenticationToken(
                        userRequest.getUsername(),
                        userRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        int number =  new Random().nextInt(900000) + 100000;
        User userLogin = userRepository.findByLogin(userRequest.getUsername()).orElse(null);
        redisTemplate.delete(userLogin.getEmail());
        redisTemplate.opsForValue().set(userLogin.getEmail(),String.valueOf(number), Duration.ofSeconds(900));
//        var res = speedSMSAPI.sendSMS(phoneNumber, number+"", 2, "0339145511");
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(userLogin.getEmail());
        message.setSubject("Mã otp đăng nhập tài khoản elearning");
        message.setText("Không gửi mã này cho bât kỳ ai, mã chỉ tồn tại trong 15 phút:" + number);
        userService.sendMail(message);
        return ResponseEntity.ok("success");
    }

    @PutMapping("change_password")
    public ResponseEntity<?> changePassword(@RequestBody ChangePasswordRequest model) throws NotActiveException {
        User user = userService.changeUser(model);
        if (Objects.isNull(user)) {
            throw new NotFoundException();
        }
        return ResponseEntity.ok(user);
    }


    @PutMapping
    public ResponseEntity<?> update(@RequestBody UpdateUserRequest model, @PathVariable("id") Long id, HttpServletRequest request) throws IOException {
        User userUpdate = userRepository.findById(id).orElseThrow(NotActiveException::new);
        String body = RequestUtil.getBody(request);
        JsonNode node = objectMapper.readTree(body);

        if (RequestUtil.existField(node, "first_name")) {
            userUpdate.setFirstName(model.getFirstName());
        }
        if (RequestUtil.existField(node, "last_name")) {
            userUpdate.setLastName(model.getLastName());
        }
        if (RequestUtil.existField(node, "activated")) {
            userUpdate.setActivated(model.isActivated());
        }
        if (RequestUtil.existField(node, "password")) {
            userUpdate.setPassword(model.getPassword());
        }
        if (RequestUtil.existField(node, "activation_key")) {
            userUpdate.setActivationKey(model.getActivationKey());
        }
        if (RequestUtil.existField(node, "authorities")) {
            userUpdate.setAuthorities(model.getAuthorities());
        }
        var response = userRepository.save(userUpdate);
        return ResponseEntity.ok(response);
    }

}
